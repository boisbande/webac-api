var mongoose = require("mongoose"),
  Schema = mongoose.Schema;


// Schema
var ProjectSchema = new Schema({
    id: Number,
    name: String,
    userId: Number,
    descriptive: String,
    languages: String,
    links: String
});

// Return model
module.exports = mongoose.model('Project', ProjectSchema);
