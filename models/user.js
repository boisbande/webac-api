var mongoose = require("mongoose"),
  Schema = mongoose.Schema;

// Schema
var UserSchema = new Schema({
    id: Number,
    email: String,
    firstName: String,
    lastName: String,
    description: String,
    salaryClaims: {
      type: Number,
      min: 10,
      max: 33000
    }
});

// Return model
module.exports = mongoose.model('User', UserSchema);
