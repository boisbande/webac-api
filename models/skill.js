var mongoose = require("mongoose"),
  Schema = mongoose.Schema;


// Schema
var SkillSchema = new Schema({
    id: Number,
    name: String,
    userId: Number,
    type: String,
    note: {
      type: Number,
      min: 0,
      max: 5
    }
});

// Return model
module.exports = mongoose.model('Skill', SkillSchema);
