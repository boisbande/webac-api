'use strict';

// Routes
module.exports = (function() {
  'use strict';
  var mongoose = require('mongoose'),
  User = require('../models/user'),
  Skill = require('../models/skill'),
  api = require('express').Router();



  api.get('/user', function (req, res) {
    User.find({}, function(err, users) {
      if (err){
        return res.status(404).send({
          message: 'Users not found',
          err: err
        });
      }
      res.json(users);
    });
  });



  api.post('/user', function (req, res) {
    var infosUser = {
      id: req.body.id,
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      description: req.body.description,
      salaryClaims: req.body.salaryClaims,
    };

    User.findOne({
      id: req.body.id,
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      description: req.body.description,
      salaryClaims: req.body.salaryClaims,
    })
    .exec(function (err, user) {
      var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (err || !emailRegex.test(req.body.email)) {
        return res.status(400).send({
          message: 'Invalid data'
        });
      }
      if (!user) {
        var newUser = new User(infosUser);
        newUser.save(function (err) {
          if (err) {
            return res.status(400).send({
              message: 'Invalid data',
              reason: err
            });
          }
          res.json(newUser);
        });
      } else {
        return res.status(401).send({
          message: 'User already exist',
          reason: err
        });
      }
    });
  });




  api.get('/user/:id', function (req, res) {
    User.findOne({ id: req.params.id })
    .exec(function (err, user) {
      if (err || !user) {
        return res.status(404).send({
          message: 'User not found'
        });
      }
      res.json(user);
    });
  });




  api.put('/user/:id', function (req, res) {
    User.findOne({ id: req.params.id })
    .exec(function (err, user) {
      if (err || !user) {
        return res.status(404).send({
          message: 'User not found'
        });
      }
      user.id = req.body.id;
      user.email = req.body.email;
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.description = req.body.description;
      user.salaryClaims = req.body.salaryClaims;
      user.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: 'Invalid data supplied',
            reason: err
          });
        }
        res.json(user);
      });
    });
  });




  api.delete('/user/:id', function (req, res) {
    User.deleteOne({ id: req.params.id })
    .exec(function (err, user) {
      if (err) {
        return res.status(404).send({
          message: 'User not found'
        });
      }
      res.json("User deleted");
    });
  });




  api.get('/user/:id/skill', function (req, res) {
    Skill.find({ userId: req.params.id })
    .exec(function (err, skills) {
      if (err || skills.length == 0) {
        return res.status(404).send({
          message: 'User not found'
        });
      }
      res.json(skills);
    });
  });




  api.get('/user/skill/:type', function (req, res) {
    Skill.find({ $or: [{ type: req.params.type }, { name: req.params.type }] })
    .exec(function (err, skills) {
      if (err || skills.length == 0) {
        return res.status(405).send({
          message: 'Invalid parameters supplied'
        });
      }
      var arrayIdUsers = []
      for (var i = 0; i < skills.length; i++) {
        arrayIdUsers.push(skills[i].userId);
      }
      User.find({ id: { $in : arrayIdUsers } })
      .exec(function (err, users) {
        if (err || users.length == 0) {
          return res.status(404).send({
            message: 'User not found'
          });
        }
        res.json(users);
      });
    });
  });




  api.get('/user/skill/:type/:note', function (req, res) {
    Skill.find({ $and: [{ $or: [{ type: req.params.type }, { name: req.params.type }] }, { note: req.params.note }] })
    .exec(function (err, skills) {
      if (err || skills.length == 0) {
        return res.status(405).send({
          message: 'Invalid parameters supplied'
        });
      }
      var arrayIdUsers = []
      for (var i = 0; i < skills.length; i++) {
        arrayIdUsers.push(skills[i].userId);
      }
      User.find({ id: { $in : arrayIdUsers } })
      .exec(function (err, users) {
        if (err || users.length === 0) {
          return res.status(404).send({
            message: 'User not found'
          });
        }
        res.json(users);
      });
    });
  });

  return api;
})();
