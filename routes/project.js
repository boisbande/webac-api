'use strict';

// Routes
module.exports = (function() {
  'use strict';
  var mongoose = require('mongoose'),
  Project = require('../models/project'),
  User = require('../models/user'),
  api = require('express').Router();



  api.get('/project', function (req, res) {
    Project.find({}, function(err, projects) {
      if (err){
        return res.status(404).send({
          message: 'Projects not found',
          err: err
        });
      }
      res.json(projects);
    });
  });



  api.post('/project', function (req, res) {
    var infosProject = {
      id: req.body.id,
      name: req.body.name,
      userId: req.body.userId,
      descriptive: req.body.descriptive,
      languages: req.body.languages,
      links: req.body.links
    };

    Project.findOne({ id: req.body.id })
    .exec(function (err, project) {
      if (err || project) {
        return res.status(405).send({
          message: 'Invalid input'
        });
      }
      var newProject = new Project(infosProject);

      User.findOne({ id: newProject.userId })
      .exec(function (err, user) {
        if (err || !user) {
          return res.status(405).send({
            message: 'Invalid input'
          });
        }
        newProject.save(function (err) {
          if (err) {
            return res.status(405).send({
              message: 'Invalid input',
              reason: err
            });
          }
          res.json(newProject);
        });
      });
    });
  });




  api.get('/project/:id', function (req, res) {
    Project.findOne({ id: req.params.id })
    .exec(function (err, project) {
      if (err || !project) {
        return res.status(404).send({
          message: 'Project not found'
        });
      }
      res.json(project);
    });
  });




  api.put('/project/:id', function (req, res) {
    Project.findOne({ id: req.params.id })
    .exec(function (err, project) {
      if (err || !project) {
        return res.status(404).send({
          message: 'Project not found'
        });
      }
      User.findOne({ id: req.body.userId })
      .exec(function (err, user) {
        if (err || !user) {
          return res.status(405).send({
            message: 'Invalid input'
          });
        }
        project.id = req.body.id;
        project.name = req.body.name;
        project.userId = req.body.userId;
        project.type = req.body.type;
        project.note = req.body.note;
        project.save(function (err) {
          if (err) {
            return res.status(405).send({
              message: 'Invalid input',
              reason: err
            });
          }
          res.json(project);
        });
      });
    });
  });




  api.delete('/project/:id', function (req, res) {
    Project.deleteOne({ id: req.params.id })
    .exec(function (err, project) {
      if (err) {
        return res.status(404).send({
          message: 'Project not found'
        });
      }
      res.json("Project deleted");
    });
  });


  return api;
})();
