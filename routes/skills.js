'use strict';

// Routes
module.exports = (function() {
  'use strict';
  var mongoose = require('mongoose'),
  Skill = require('../models/skill'),
  User = require('../models/user'),
  api = require('express').Router();



  api.post('/skill', function (req, res) {
    var infosSkill = {
      id: req.body.id,
      name: req.body.name,
      userId: req.body.userId,
      type: req.body.type,
      note: req.body.note
    };
    Skill.findOne({ id: req.body.id })
    .exec(function (err, skill) {
      if (err || skill) {
        return res.status(405).send({
          message: 'Invalid input'
        });
      }
      var newSkill = new Skill(infosSkill);

      User.findOne({ id: newSkill.userId })
      .exec(function (err, user) {
        if (err || !user) {
          return res.status(405).send({
            message: 'Invalid input'
          });
        }
        newSkill.save(function (err) {
          if (err) {
            return res.status(405).send({
              message: 'Invalid input',
              reason: err
            });
          }
          res.json(newSkill);
        });
      });
    });
  });



  api.get('/skill/:id', function (req, res) {
    Skill.findOne({ id: req.params.id })
    .exec(function (err, skill) {
      if (err || !skill) {
        return res.status(404).send({
          message: 'Skill not found'
        });
      }
      res.json(skill);
    });
  });



  api.put('/skill/:id', function (req, res) {
    Skill.findOne({ id: req.params.id })
    .exec(function (err, skill) {
      if (err || !skill) {
        return res.status(404).send({
          message: 'Skill not found'
        });
      }
      skill.id = req.body.id;
      skill.name = req.body.name;
      skill.userId = req.body.userId;
      skill.type = req.body.type;
      skill.note = req.body.note;
      skill.save(function (err) {
        if (err) {
          return res.status(400).send({
            message: 'Can\'t save this skill',
            reason: err
          });
        }
        res.json(skill);
      });
    });
  });



  api.delete('/skill/:id', function (req, res) {
    Skill.deleteOne({ id: req.params.id })
    .exec(function (err, project) {
      if (err) {
        return res.status(404).send({
          message: 'Skill not found'
        });
      }
      res.json("Skill deleted");
    });
  });


  return api;
})();
