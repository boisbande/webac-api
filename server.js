// Dependencies
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// MongoDB
mongoose.connect('mongodb://localhost/rest_test');

// Express
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes
var user = require('./routes/user');
var project = require('./routes/project');
var skill = require('./routes/skills');

app.use('/api', user);
app.use('/api', project);
app.use('/api', skill);

// Start server
app.listen(3000);
